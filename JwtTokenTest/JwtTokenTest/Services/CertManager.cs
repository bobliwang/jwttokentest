﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace JwtTokenTest.Services
{
    public class CertManager
    {
        public X509Certificate2 LoadCertificate(string thumbprint, StoreName storeName = StoreName.TrustedPeople, StoreLocation storeLocation = StoreLocation.LocalMachine)
        {
            X509Certificate2 cert = null;
            X509Store store = null;
            try
            {
                store = new X509Store(storeName, storeLocation);
                store.Open(OpenFlags.ReadOnly);

                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
                if (certs != null && certs.Count > 0)
                {
                    cert = certs[0];
                }
                else
                {
                    throw new IndexOutOfRangeException("Certificate not found");
                }
            }
            finally
            {
                store.Close();
            }

            return cert;
        } 
    }
}