﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Configuration;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using JwtTokenTest.Services;

namespace JwtTokenTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var certMgr = new CertManager();
            
            var cert = certMgr.LoadCertificate("6530A4BF798B028290B116BE2C49C5BD905906FA");


            var claims = new[] { new Claim(PrivateClaimTypes.Usg, "S2S") };

            var identityConfiguration = new IdentityConfiguration();
            ////identityConfiguration.SecurityTokenHandlers.Configuration.AudienceRestriction.AllowedAudienceUris.Add(new Uri("urn:pdfprinttest.nintextest.com"));
            var tokenHandler = new JwtSecurityTokenHandler { Configuration = identityConfiguration.SecurityTokenHandlers.Configuration };

            X509SigningCredentials signingCert = new X509SigningCredentials(cert);
            var token = new JwtSecurityToken(
                issuer: "testIssuer1", // DefaultIssuer,
                audience: "testAudience1",
                claims: claims.ToArray(),
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(10)

                ,signingCredentials: signingCert
            ) as SecurityToken;




            var jwtToken = tokenHandler.WriteToken(token);


            var token1 = new JwtSecurityToken(jwtToken);

            var aud = token1.Audiences;

            ValidateTokenWithX509SecurityToken(new X509RawDataKeyIdentifierClause(cert), jwtToken);
            try
            {
                
                tokenHandler.ValidateToken(token1);
            }
            catch (Exception ex)
            {
            } 

            
        }

        static ClaimsPrincipal ValidateTokenWithX509SecurityToken(X509RawDataKeyIdentifierClause x509DataClause, string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var x509SecurityToken = new X509SecurityToken(new X509Certificate2(x509DataClause.GetX509RawData()));
            var validationParameters = new TokenValidationParameters
            {
                AudienceValidator = (audiences, securityToken, validationParams) => true,
                IssuerSigningToken = x509SecurityToken,
                ValidIssuer = "testIssuer1",
            };


            SecurityToken outputToken;
            ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out outputToken);

            return claimsPrincipal;
        }
         
    }

    public class AzureConfigurationBasedIssuerRegistry : ConfigurationBasedIssuerNameRegistry
    {
        public override void LoadCustomConfiguration(System.Xml.XmlNodeList customConfiguration)
        {
            ////foreach (var certificate in Identity.TrustedCertificates)
            ////{
            ////    var thumbPrint = configurationManager.GetSetting(string.Concat(certificate.Name, "_Thumbprint"));
            ////    if (string.IsNullOrWhiteSpace(thumbPrint))
            ////    {
            ////        throw new Exception(string.Format("Failed to load thumbprint for {0}.", certificate));
            ////    }

            ////    this.AddTrustedIssuer(thumbPrint, certificate.Name);
            ////}
        }
    }

    public static class PrivateClaimTypes
    {
        /// <summary>
        /// Platform product tenant ID.
        /// </summary>
        public const string PptId = "http://www.nintex.com/2013/05/pptid";

        /// <summary>
        /// Token Usage.
        /// </summary>
        public const string Usg = "http://www.nintex.com/2013/05/usg";
    }

    public enum JwtHashAlgorithm
    {
        RS256,
        HS384,
        HS512
    }
}
