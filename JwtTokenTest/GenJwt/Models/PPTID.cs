﻿namespace GenJwt.Models
{
    using System;

    public class PPTID
    {
        private const string APPLICATION_ID = "F";
        private const string PLATFORM_ID = "SPO";

        private string _customerId;

        public PPTID(string customerId)
        {
            _customerId = customerId;
        }

        public string Value
        {
            get
            {
                //_customerId = new Guid("465b2e3a-3a7d-4838-a44c-0aba8a06f502");
                //return "S:NWO:12345";
                //return "S:NWO:SIDTest02";
                //return "S:NWO:MondayArvo";
                //return string.Format("{0}:{1}:{2}", PLATFORM_ID, APPLICATION_ID, _customerId.ToString("N"));
                return string.Format("{0}:{1}:{2}", PLATFORM_ID, APPLICATION_ID, _customerId);
            }
        }

        public string CustomerID
        {
            get
            {
                return _customerId;
            }
        }

        public override string ToString()
        {
            return Value;
        }

        public static PPTID Parse(string input)
        {
            var parts = input.Split(':');
            if (parts.Length != 3)
            {
                throw new ArgumentOutOfRangeException();
            }

            return new PPTID(parts[2]);
        }
    }
}