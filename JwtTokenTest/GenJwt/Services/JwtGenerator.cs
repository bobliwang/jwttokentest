﻿namespace GenJwt.Services
{
    using System;
    using System.Security.Claims;
    using System.Security.Cryptography.X509Certificates;
    using Models;

    public class JwtGenerator
    {
        private X509Certificate2 cert;

        public JwtGenerator(X509Certificate2 cert)
        {
            this.cert = cert;
        }

        public string CreateS2SToken(string audienceUri, PPTID pptId = null, DateTime? expiry = null, string issuer = Issuers.Spolf)
        {
            var claims = new[] { new Claim(PrivateClaimTypes.Usg, UsageClaimValues.ServerToServer) };

            if (pptId != null)
            {
                return TokenHelper.CreateTokenWithPptIdClaim(cert, audienceUri, claims, pptId.Value, expiry, issuer);
            }

            return TokenHelper.CreateToken(cert, audienceUri, claims, expiry);
        }
        
        public static class UsageClaimValues
        {
            public const string ServerToServer = "s2s";

            public const string UserInterface = "ui";
        }

        public static class PrivateClaimTypes
        {
            /// <summary>
            /// Platform product tenant ID.
            /// </summary>
            public const string PptId = "http://www.nintex.com/2013/05/pptid";

            /// <summary>
            /// Token Usage.
            /// </summary>
            public const string Usg = "http://www.nintex.com/2013/05/usg";
        }

        public static class Issuers
        {
            public const string Spolf = "urn:nintex:spolf";

            public const string Spolw = "urn:nintex:spolw";
        }
    }
}