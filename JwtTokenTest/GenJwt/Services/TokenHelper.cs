﻿namespace GenJwt.Services
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Configuration;
    using System.IdentityModel.Protocols.WSTrust;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Web;

    public class TokenHelper
    {
        /// <summary>
        /// Creates a token with a cert, audience and sets the identity from the thread current principal as the NameIdentifier claim with additional claims.
        /// Also creates the PPT ID claim.
        /// The first entry of the audenice in configuration is used as issuer.
        /// </summary>
        /// <param name="certificate">The X509 cert.</param>
        /// <param name="audience">The audience of the token.</param>
        /// <param name="claims">The list of claims to add to the token.</param>
        /// <returns>Returns a token.</returns>
        public static string CreateTokenWithPptIdClaim(X509Certificate2 certificate, string audience, IEnumerable<Claim> claims, string pptId, DateTime? expiry = null, string issuer = null)
        {
            claims = EnsurePptIdClaim(claims, pptId);

            return JwtHelper.CreateToken(certificate, audience, claims, expiry, issuer);
        }

        /// <summary>
        /// Creates a token with a cert, audience and sets the identity from the thread current principal as the NameIdentifier claim with additional claims.
        /// Also creates the PPT ID claim.
        /// The first entry of the audenice in configuration is used as issuer.
        /// </summary>
        /// <param name="certificate">The X509 cert.</param>
        /// <param name="audience">The audience of the token.</param>
        /// <param name="claims">The list of claims to add to the token.</param>
        /// <returns>Returns a token.</returns>
        public static string CreateToken(X509Certificate2 certificate, string audience, IEnumerable<Claim> claims, DateTime? expiry = null)
        {
            if (claims == null)
            {
                claims = new Claim[0];
            }

            return JwtHelper.CreateToken(certificate, audience, claims, expiry);
        }

        /// <summary>
        /// Creates a token with a cert, audience and the identity from the thread current principal as the NameIdentifier claim and creates the PPT ID claim.
        /// The first entry of the audenice in configuration is used as issuer.
        /// </summary>
        /// <param name="certificate">The X509 cert.</param>
        /// <param name="audience">The audience of the token.</param>
        /// <returns>Returns a token.</returns>
        public static string CreateTokenWithPptIdClaim(X509Certificate2 certificate, string audience, string pptId)
        {
            IEnumerable<Claim> claims = EnsurePptIdClaim(null, pptId);

            return JwtHelper.CreateToken(certificate, audience, claims);
        }

        private static IEnumerable<Claim> EnsurePptIdClaim(IEnumerable<Claim> claims, string pptId)
        {
            var pptIdClaim = new Claim(JwtGenerator.PrivateClaimTypes.PptId, pptId);

            return claims == null ? new List<Claim> { pptIdClaim } : new List<Claim>(claims) { pptIdClaim };
        }
    }

    public class JwtHelper
    {
        /// <summary>
        /// Creates a token with a cert, audience, sets the claims and the token issuer.
        /// </summary>
        /// <param name="issuer">The issuer of the token. Leaving this empty or null would use the first entry of the audenice in configuration as issuer.</param>
        /// <param name="certificate">The X509 cert.</param>
        /// <param name="audience">The audience of the token.</param>
        /// <param name="claims">The list of claims to add to the token.</param>
        /// <returns>Returns a token.</returns>
        public static string CreateToken(X509Certificate2 certificate, string audience, IEnumerable<Claim> claims, DateTime? expiry = null, string issuer = "urn:nintex:spolf")
        {
            claims = EnsureClaims(claims);

            var identityConfiguration = new IdentityConfiguration();
            ////identityConfiguration.SecurityTokenHandlers.Configuration.AudienceRestriction.AllowedAudienceUris.Add(new Uri("urn:pdfprinttest.nintextest.com"));
            var tokenHandler = new JwtSecurityTokenHandler { Configuration = identityConfiguration.SecurityTokenHandlers.Configuration };

            var token = new JwtSecurityToken(
                issuer,
                audience,
                claims.ToArray(),
                null,
                expiry ?? DateTime.UtcNow.AddMinutes(10),
                new X509SigningCredentials(certificate));

            return tokenHandler.WriteToken(token);
        }

        private static IEnumerable<Claim> EnsureClaims(IEnumerable<Claim> claims)
        {
            return claims ?? new Claim[0];
        }
    }
}