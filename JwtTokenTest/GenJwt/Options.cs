﻿namespace GenJwt
{
    public class Options
    {
        public string Audience { get; set; }

        public string CertPath { get; set; }

        public string CertPassword { get; set; }

        public string PptId { get; set; }

        public string Issuer { get; set; }

        public string ExpireInMinutesStr { get; set; }
    }
}