﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenJwt
{
    using NDesk.Options;
    using Services;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Run(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}",  ex);
            }
        }

        private static void Run(string[] args)
        {
            // -aud:urn:nintex:spolfprint
            // -cert:c:\temp\tstfol.pfx
            // -ppt:875be72c-0b15-4ec8-acab-a4839ce55862
            // -issuer:urn:nintex:spolf
            // -expire:5 - minutes
            Options options = new Options();

            if (!ExtractOptionValues(args, options))
            {
                Console.WriteLine("Arguments wrong. Exit");

                return;
            }

            Console.WriteLine($"audience: {options.Audience}");
            Console.WriteLine($"certPath: {options.CertPath}");
            Console.WriteLine($"certPassword: {options.CertPassword}");
            Console.WriteLine($"pptid: {options.PptId}");
            Console.WriteLine($"issuer: {options.Issuer}");

            var exprMinutes = 5;
            int.TryParse(options.ExpireInMinutesStr, out exprMinutes);
            Console.WriteLine($"expireInMinutes: {exprMinutes}");

            //Console.ReadLine();

            var cert = new CertManager().LoadCertificate(options.CertPath, options.CertPassword);
            Console.WriteLine($"cert thumb: {cert.Thumbprint}");

            var token = new JwtGenerator(cert).CreateS2SToken(
                audienceUri: options.Audience,
                pptId: options.PptId != null ? new Models.PPTID(options.PptId) : null,
                expiry: DateTime.UtcNow.AddMinutes(exprMinutes),
                issuer: options.Issuer);

            Console.WriteLine("");
            Console.WriteLine("-------------------Generated JWT Begins-------------------");
            Console.WriteLine(token);
            Console.WriteLine("-------------------Generated JWT Ends-------------------");
        }

        private static bool ExtractOptionValues(string[] args, Options options)
        {
            if (args == null || !args.Any())
            {
                Console.WriteLine("Warning: No args are sepcified, will use default.");

                options.Audience = "urn:nintex:spolfprint";
                options.CertPath = @"C:\Users\WangL\Downloads\Certs\DEV\TSTFOL.pfx";
                options.CertPassword = "cAMEve7a";
                options.Issuer = "urn:nintex:spolf";
                options.ExpireInMinutesStr = "5";

                return true;
            }

            foreach (var arg in args)
            {
                var pos = arg.IndexOf(":");

                if (pos < 0)
                {
                    Console.WriteLine("Wrong arg '{0}'. Each arg should be in '-(argName):(value)' format", arg);
                    return false;
                }

                var argName = arg.Substring(0, pos);
                if (argName.StartsWith("-"))
                {
                    argName = argName.Substring(1);
                }

                string shortArgName = argName;
                if (argName.Length > 3)
                {
                    shortArgName = argName.Substring(0, 3);
                }

                shortArgName = shortArgName.ToLowerInvariant();

                switch (shortArgName)
                {
                    case "aud":
                        options.Audience = arg.Substring(pos + 1);
                        break;
                    case "cer":
                        options.CertPath = arg.Substring(pos + 1);
                        break;
                    case "cpw":
                        options.CertPassword = arg.Substring(pos + 1);
                        break;
                    case "ppt":
                        options.PptId = arg.Substring(pos + 1);
                        break;
                    case "iss":
                        options.Issuer = arg.Substring(pos + 1);
                        break;
                    case "exp":
                        options.ExpireInMinutesStr = arg.Substring(pos + 1);
                        break;
                    default:
                        Console.WriteLine("not supported arg name '{0}'", argName);
                        return false;
                }
            }

            return true;
        }
    }
}
