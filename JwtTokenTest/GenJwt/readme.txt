﻿1. Usage
.\GenJwt.exe -aud:$(audienceUrl) -cer:$(certPFXFilePath) -cpw:$(certPassword)  -iss:$(issuer) -exp:$(expireInMinutes) -ppt:$(pptId [optional]) > output.txt

2. Example Usage
.\GenJwt.exe -aud:urn:nintex:spolfprint "-cer:C:\Users\WangL\Downloads\Certs\DEV\TSTFOL.pfx" -cpw:cAMEve7a  -iss:urn:nintex:spolf -exp:5 > output.txt

3. Example Results

audience: urn:nintex:spolfprint
certPath: C:\Users\WangL\Downloads\Certs\DEV\TSTFOL.pfx
certPassword: cAMEve7a
pptid: 
issuer: urn:nintex:spolf
expireInMinutes: 5
cert thumb: 6530A4BF798B028290B116BE2C49C5BD905906FA

-------------------Generated JWT Begins-------------------
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlpUQ2t2M21MQW9LUXNSYS1MRW5GdlpCWkJ2byJ9.eyJodHRwOi8vd3d3Lm5pbnRleC5jb20vMjAxMy8wNS91c2ciOiJzMnMiLCJpc3MiOiJ1cm46bmludGV4OnNwb2xmIiwiYXVkIjoidXJuOm5pbnRleDpzcG9sZnByaW50IiwiZXhwIjoxNDY4NTU3OTQ1fQ.hmVx5865WpzI2mBj0VE1PH8Fu6CEDTVIwSK-HXs4sLEV1N96VFJxtoaUMB2EZjkgqxJ7z5_pXw92kAR52dHnj-syr76IMbyj6lAznqxquFBtbbE7Nnv0GgmP5aSLXwVpEldvVZL9W_ZoMdZaRw-TNY98WKp7rxrSz4D_WZoLVYm7vpfAmYzKFOATu4f5xp_2E5A567NexWZgVDt7qFoFrZqKyW04xnEf4csgBTtL45jqMafmXWaUpci0MbPay6rf_dzefHTTOBeMctxnorHTEezV6NAxtyXt1KMlGoTG-r1PTX8DXBAFyZFwuEaoKoVoaQVNamy3woa9F1xKPgDtvg
-------------------Generated JWT Ends-------------------


