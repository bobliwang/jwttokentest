﻿using System.Web;
using System.Web.Mvc;

namespace WebAppCertTest
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
    }
  }
}
