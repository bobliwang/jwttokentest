﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using WebAppCertTest.Extensions;
using WebAppCertTest.Services;

namespace WebAppCertTest.Controllers
{
    public class TokenGenController : Controller
    {
        // GET: TokenGen
        [HttpGet]
        public ActionResult Index()
        {
          var thumbprint = ConfigurationManager.AppSettings["cert_Thumbprint"];

          try
          {
            var certMgr = new CertManager();
            var cert = certMgr.LoadCertificate(thumbprint, StoreName.My, StoreLocation.CurrentUser);


            var claims = new[] {new Claim(PrivateClaimTypes.Usg, "S2S")};

            var identityConfiguration = new IdentityConfiguration();
            ////identityConfiguration.SecurityTokenHandlers.Configuration.AudienceRestriction.AllowedAudienceUris.Add(new Uri("urn:pdfprinttest.nintextest.com"));
            var tokenHandler = new JwtSecurityTokenHandler
            {
              Configuration = identityConfiguration.SecurityTokenHandlers.Configuration
            };

            X509SigningCredentials signingCert = new X509SigningCredentials(cert);
            var token = new JwtSecurityToken(
              issuer: "testIssuer1", // DefaultIssuer,
              audience: "testAudience1",
              claims: claims.ToArray(),
              notBefore: DateTime.UtcNow,
              expires: DateTime.UtcNow.AddMinutes(5)

              , signingCredentials: signingCert
              );

            var jwtToken = tokenHandler.WriteToken(token as SecurityToken);

            return this.Json(new {jwtToken}, JsonRequestBehavior.AllowGet);
          }
          catch (Exception ex)
          {
            return this.Json(new { thumbprint, error = ex.GetFullStackTrace() }, JsonRequestBehavior.AllowGet);
          }
        }

      [HttpPost]
      public ActionResult Validate(string token)
      {
        
        var thumbprint = ConfigurationManager.AppSettings["cert_Thumbprint"];
        
        try
        {
          var tokenHandler = new JwtSecurityTokenHandler();
          var certMgr = new CertManager();
          var cert = certMgr.LoadCertificate(thumbprint, StoreName.My, StoreLocation.CurrentUser);

          var x509DataClause = new X509RawDataKeyIdentifierClause(cert);
          var x509SecurityToken = new X509SecurityToken(new X509Certificate2(x509DataClause.GetX509RawData()));
          var validationParameters = new TokenValidationParameters
          {
            AudienceValidator = (audiences, securityToken, validationParams) => true,
            IssuerSigningToken = x509SecurityToken,
            ValidIssuer = "testIssuer1",
            RequireExpirationTime = true,
            ValidateLifetime = true,
            LifetimeValidator =  (DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParams) => (expires ?? DateTime.MaxValue) > DateTime.UtcNow
          };
        
          SecurityToken outputToken;
          ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out outputToken);

          return this.Json(new {isValid = true});
        }
        catch (Exception ex)
        {
          return this.Json(new { isValid = false, error = ex.GetFullStackTrace() });
        }
      }
    }
}