﻿using System.Web.Mvc;

namespace WebAppCertTest.Controllers
{
  public class OauthController : Controller
  {
    public ActionResult Privacy()
    {
      return this.Content("This is Privacy page");
    }

    public ActionResult Accepted()
    {
      return this.Content("This is Accepted page");
    }
  }
}