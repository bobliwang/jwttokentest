﻿using System;
using System.Text;

namespace WebAppCertTest.Extensions
{
  public static class ExceptionExtensions
  {
    public static string GetFullStackTrace(this Exception ex)
    {
      var sb = new StringBuilder();

      while (ex != null)
      {
        sb.AppendLine(ex.GetType().FullName + " - " + ex.Message + " => " + ex.StackTrace);
        sb.AppendLine("--------------------");
        ex = ex.InnerException;
      }

      return sb.ToString();
    }
  }
}