﻿namespace WebAppCertTest.Services
{
  public class PrivateClaimTypes
  {
    /// <summary>
    /// Platform product tenant ID.
    /// </summary>
    public const string PptId = "http://www.nintex.com/2013/05/pptid";

    /// <summary>
    /// Token Usage.
    /// </summary>
    public const string Usg = "http://www.nintex.com/2013/05/usg"; 
  }
}